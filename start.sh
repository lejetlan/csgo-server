#!/bin/bash

if [ -z $SERVER_TOKEN ]
then
   echo "Warning: No server token supplied" >&2
fi

[[ $GAME_TYPE -ge 1 ]] && GAME_TYPE="1" || GAME_TYPE="0"
[[ $GAME_MODE -ge 1 ]] && GAME_MODE="1" || GAME_MODE="0"

if [[ $SERVER_TYPE == "PUG" ]]
then
   cp -R /steam/server/plugins/sourcemod/.  /steam/server/csgo
   cp -R /steam/server/plugins/pugsetup/. /steam/server/csgo
elif [[ $SERVER_TYPE == "Tournament" ]]
then
   if [[ $TOURNAMENT_SYSTEM == "Get5" ]]
   then
      cp -R /steam/server/plugins/sourcemod/. /steam/server/csgo
      cp -R /steam/server/plugins/get5/. /steam/server/csgo
   fi
else
   echo "ENV 'SERVER_TYPE' is not set - defaulting to BareMetal"
fi

# Command line
[[ -n $GAME_TYPE ]] && GAME_TYPE="+game_type $GAME_TYPE"
[[ -n $GAME_MODE ]] && GAME_MODE="+game_mode $GAME_MODE"
[[ -n $MAP ]] &&  MAP="+map $MAP"
[[ -n $MAXPLAYERS ]] &&  MAXPLAYERS="-maxplayers_override $MAXPLAYERS"
[[ -n $SERVER_TOKEN ]] && SERVER_TOKEN="+sv_setsteamaccount $SERVER_TOKEN"
[[ -n $WORKSHOP_COLLECTION ]] && WORKSHOP_COLLECTION="+host_workshop_collection $WORKSHOP_COLLECTION"
[[ -n $WORKSHOP_START_MAP ]] && WORKSHOP_START_MAP="+workshop_start_map $WORKSHOP_START_MAP"
[[ -n $AUTHKEY ]] && AUTHKEY="-authkey $AUTHKEY"
[[ -n $TICKRATE ]] && TICKRATE="-tickrate $TICKRATE"

# Config file
[[ -n $RCON_PASSWORD ]] && RCON_PASSWORD="rcon_password $RCON_PASSWORD"
[[ -n $SV_PASSWORD ]] && SV_PASSWORD="sv_password $SV_PASSWORD"
[[ -z $HOSTNAME ]] && HOSTNAME="CS Server hosted by FinX"


cat <<EOF > /steam/server/csgo/cfg/server.cfg

hostname "$HOSTNAME"
$RCON_PASSWORD
$SV_PASSWORD

EOF

cat /steam/server/csgo/cfg/server.cfg.template >> /steam/server/csgo/cfg/server.cfg

cat <<EOF >> /steam/server/csgo/cfg/autoexec.cfg

hostname "$HOSTNAME"
$RCON_PASSWORD
$SV_PASSWORD

EOF

exec ./srcds_run -game csgo +sv_lan 0 -ip 0.0.0.0 +net_public_adr 0.0.0.0 $MAP -usercon $GAME_MODE $GAME_TYPE $TICKRATE $MAXPLAYERS $SERVER_TOKEN $WORKSHOP_COLLECTION $WORKSHOP_START_MAP $AUTHKEY

