#!/bin/bash
VERSION=$(date +%Y%m%d-%H%M%S)
docker build -t registry.npf.dk/csgo-server --no-cache .
docker tag registry.npf.dk/csgo-server registry.npf.dk/csgo-server:$VERSION

#if [ $1 = "push" ]
#then
#  docker push registry.npf.dk/csgo-server
#  docker push registry.npf.dk/csgo-server:$VERSION
#fi
