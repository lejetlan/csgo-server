## Dockerized CSGO server

Base CSGO start script which supports these environment variables:

```
docker run -d \
	--name csgo-srv01 \
	-p 27015:27015 \
	-p 27015:27015/udp \
	-p 27020:27020/udp \
	-e "HOSTNAME=CSGO Server | Hosted by FinX" \
	-e "TICKRATE=128" \
	-e "RCON_PASSWORD=ADMIN-PASSWORD" \
	-e "SV_PASSWORD=PASSWORD" \
	-e "GAME_TYPE=0" \
	-e "GAME_MODE=1" \
	-e "MAP=de_nuke" \
	-e "MAXPLAYERS=12" \
	-e "SERVER_TOKEN=<GLST TOKEN GOES HERE>" \
	-e "SERVER_TYPE=<PUG/Tournament>" \
	-e "TOURNAMENT_SYSTEM=<eBot/Get5/BareMetal>" \
	strand/gaas-csgo
```

