FROM strand/gaas-steamcmd
USER steam
WORKDIR /steam/steamcmd_linux
RUN mkdir -p /steam/server \
  && ./steamcmd.sh +login anonymous +force_install_dir ../server +app_update 740 +quit

ARG CACHE_DATE
# need to run docker build --build-arg CACHE_DATE="$(date)"
RUN ./steamcmd.sh +login anonymous +force_install_dir ../server +app_update 740 +quit

ADD start.sh /steam/server/

ADD cfg/* /steam/server/csgo/cfg/
COPY plugins/ /steam/server/plugins/
USER root
RUN /bin/bash -c 'chown -R steam:steam /steam/server/csgo/cfg' \
  && /bin/bash -c 'chown -R steam:steam /steam/server/plugins' \
  && /bin/bash -c 'chmod -R 755 /steam/server/plugins'

USER steam
WORKDIR /steam/server
CMD ["./start.sh"]
